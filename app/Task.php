<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    protected $table = "tasks";

    protected $guarded = [];


    // pridedame laukelius
	protected $dates = ['deadline'];


	protected $attributes = [
        'status' => 2
    ];

    public function getStatusAttribute($attribute)
    {
        return $this->statusOptions()[$attribute];
    }

    public function user() {
        return $this->belongsTo(User::class);
    }

    public static function uncompleted() {
    	return Task::where('status', '=', 0)->get();
	}

    public function scopePending($query)
    {
        return $query->where('status', 2);
    }

    public function scopeCompleted($query)
    {
        return $query->where('status', 1);
    }

    public function scopeUncompleted($query)
    {
        return $query->where('status', 0);
    }

    public function statusOptions()
    {
        return [
            2 => 'Pending',
            1 => 'Completed',
            0 => 'Uncompleted',
        ];
    }
}
