<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Task;
use App\User;

class TasksController extends Controller
{

    public function __construct() {

        
        $this->middleware('auth');
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tasks = Task::all();
        
        return view('tasks.index', compact('tasks'));
    }

    public function uncompleted()
    {
        $uncompletedTasks = Task::uncompleted();
        $users = User::all();
        
        return view('tasks.uncompleted', compact('uncompletedTasks', 'tasks'));
    }



    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $users = User::all();
        $task = new Task();

        return view('tasks.create', compact('users', 'task'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Task::create($this->validateRequest());
       
        return redirect()->route('tasks.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $task = Task::findOrFail($id);
        return view('tasks.show', compact('task'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $task = Task::findOrFail($id);
        $users = User::all();
		return view('tasks.edit', compact('task', 'users'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $task = Task::findOrFail($id);
        $task->update($this->validateRequest());

        return redirect()->route('tasks.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $task = Task::findOrFail($id);
        $task->delete();
        return redirect()->route('tasks.index');
    }


    private function validateRequest() {
        return request()->validate([
            'name' => 'required|min:3',
            'description' => 'required|max:255',
            'user_id' => 'required',
            'deadline' => 'required',
            'status' => 'required',
            
        ]);
    }


    public function changeStatus($id) {
		$tasks = Task::find($id);

		if($tasks->status == 'Completed') {
			$tasks->status = 0;
		} else if($tasks->status == 'Pending' || $tasks->status == 'Uncompleted') {
			$tasks->status = 1;
		}

		$tasks->save();

		return redirect()->back();
	}




}
