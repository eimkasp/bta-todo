@extends('layouts.app')

@section('title', 'Vartotojai')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-12">

            <h1>Users' List</h1>

        </div>
    </div>
</div>
<div class="container">
    @foreach($users as $user)
    <div class="row">
        <div class="col-2">
            <a href="{{ route('users.show',$user->id) }}">{{ $user->name}} </a>
        </div>
        <div class="col-2">
            <form action="{{ route('users.destroy',$user->id) }}" method="POST">
                @csrf
                @method('DELETE')

                <button type="submit" class="btn btn-danger">Ištrinti</button>
            </form>
        </div>
        <div class="col-2">
            <a class="btn btn-primary" href="{{ route('users.edit',$user->id) }}">Redaguoti</a>
        </div>


    </div>
    @endforeach
</div>
@endsection