@extends('layouts.app')


@section('content')

<div class="container">

    <div class="row">
        <div class="col-12">
            <h1>{{ $user->name }}</h1>

            <div>
                Atliktos uzduotys: {{ $user->completed_tasks->count() }}
            </div>

            <div>
                Neatliktos uzduotys: {{ $user->pending_tasks->count() }}
            </div>
            <hr>

            <h3>Uzduotys</h3>

            <ul>
                @foreach($user->tasks as $task)
                    <li>{{ $task->name }}</li>
                @endforeach
            </ul>

        </div>
    </div>
</div>
@endsection