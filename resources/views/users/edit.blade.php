@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-sm-12">
            <h1>Redaguoti duomenis: {{ $user->name }}</h1>
        </div>
        <div class="col-sm-12">
            <form method="POST" action="{{ route('users.update', $user->id) }}">
                @csrf
                @method('PUT')

                <div class="form-group">
                    <label>Name</label>
                    <input type="text" name="name" value="{{ $user->name }}" class="form-control" />
                </div>

                <div class="form-group">
                    <label>E-mail</label>
                    <input type="text" name="email" value="{{ $user->email }}" class=" form-control" />
                </div>

                


                <div class="form-group">
                    <input type="submit" class="btn btn-success" value="Redaguoti" />
                </div>

            </form>
        </div>
    </div>
</div>

@endsection