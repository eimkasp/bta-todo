@extends('layouts.app')

@section('title', 'Užduotys')

@section('content')
<div class="row">
    <div class="col-12">

        <h1>Tasks' List</h1>
        <a class="btn btn-success" href="{{ route('tasks.create') }}">Pridėti naują uzduoti</a>
    </div>
</div>

@foreach($tasks as $task)
<div class="row">
    <div class="col-2">
    <a href="{{ route('tasks.show',$task->id) }}">{{ $task->name}} </a>
    </div>
    <div class="col-4">
        {{ $task->user->name}}
    </div>
    <div class="col-4">
        {{ $task->deadline->format('Y-m-d') }}
    </div>
    <div class="col-2">
        {{ $task->status }}
    </div>

</div>
@endforeach

@endsection

