@extends('layouts.app')

@section('title', 'Užduotys')

@section('content')
<div class="row">
    <div class="col-12">

        <h1>Uncompleted Tasks' List</h1>
        <ul>
            @foreach($uncompletedTasks as $uncompletedTask)
            <li><a href="{{ route('tasks.show',$uncompletedTask->id) }}">{{ $uncompletedTask->name}} </a></li>

            @endforeach
        </ul>
    </div>
</div>
@endsection