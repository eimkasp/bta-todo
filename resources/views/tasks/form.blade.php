<div class="form-group">
    <label for="name">Name:</label>
    <input class="form-control" type="text" name="name" value="{{ old('name') ?? $task->name }}">
</div>


<div class="form-group">
    <label for="description">Description:</label>
    <textarea name="description" id="description" class="form-control">{{ old('description') ?? $task->description }}</textarea>
</div>

<div class="form-group">
<label for="user">Kam priskirta užduotis:</label>
    <select class="form-control" name="user_id">
        @foreach($users as $user)
        <option value="{{ $user->id }}">
            {{ $user->name }}
        </option>
        @endforeach
    </select>
</div>

<div class="form-group">
    <label>Data iki kuriuos reikia užbaigti užduotį:</label>
    <input type="date" name="deadline" value="{{ old('deadline') ?? $task->deadline }}" class="form-control" />
</div>

<div class="form-group">
    <label for="status">Užduoties statusas:</label>
    <select name="status" id="status" class="form-control">
        <option value="" disabled>Užduoties būsena</option>
        @foreach($task->statusOptions() as $statusOptionKey => $statusOptionValue)
            <option value="{{ $statusOptionKey }}" {{ $task->status == $statusOptionValue ? 'selected' : '' }}>{{ $statusOptionValue }}</option>
        @endforeach
    </select>
</div>


@csrf