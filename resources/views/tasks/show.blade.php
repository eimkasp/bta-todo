@extends('layouts.app')

@section('title', 'Uzduotis: ' . $task->name)

@section('content')

    <div class="row">
        <div class="col-12">
            <h1>Uzduotis: {{ $task->name}}</h1>
            <p><a class="btn btn-primary" href="{{ route('tasks.edit',$task->id) }}">Redaguoti</a></p>
            <form action="{{ route('tasks.destroy',$task->id) }}" method="POST">
                @csrf
                @method('DELETE')
                <button type="submit" class="btn btn-danger">Ištrinti</button>
            </form>

            @if($task->status == 'Completed')
                <a href="{{ route('task.change-status', $task->id) }}" class="btn btn-success mt-3">
                    Pazymeti kaip neatlikta
                </a>
            @else
                <a href="{{ route('task.change-status', $task->id) }}" class="btn btn-success mt-3">
                    Pazymeti kaip atlikta
                </a>
            @endif


        </div>
    </div>

    <hr>

    <div class="row">
        <div class="col-12">
            <p><strong>Name</strong> {{ $task->name }}</p>
            <p><strong>Status</strong> {{ $task->status }}</p>
            <p><strong>Company</strong> {{ $task->user->name }}</p>

        </div>
    </div>
@endsection