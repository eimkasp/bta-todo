@extends('layouts.app')

@section('title', 'Redaguoti: ' . $task->name . 'užduotį')

@section('content')

<div class="row">
    <div class="col-12">

        <h1> užduoties: {{ $task->name }} redagavimas </h1>
    </div>
</div>

<div class="row">
    <div class="col-12">

        <form method="POST" action="{{ route('tasks.update', $task->id) }}">
            @method('PATCH')
            @include('tasks.form')

            <button type="submit" class="btn btn-primary">Save Task</button>
        </form>
    </div>
</div>

<script>
    CKEDITOR.replace('description');
</script>

@endsection