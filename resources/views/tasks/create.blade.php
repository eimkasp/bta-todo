@extends('layouts.app')

@section('title', 'Naujos užduoties sukūrimas')

@section('content')

<div class="container">

    <div class="col-12">
        <form  method="POST" action="{{ route('tasks.store') }}">


            @include('tasks.form')

            <button type="submit" class="btn btn-primary">Add Customer</button>
        </form>
    </div>
</div>


<script>
    CKEDITOR.replace('description');
</script>
@endsection